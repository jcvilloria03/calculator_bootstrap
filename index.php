
<head>
  <title>Bootstrap</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 15px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc;
}

.panel {
    padding: 0 15px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}
</style>
</head>
<?php

?>
<form action="#" method="post" id="checkForm" name="checkForm" target="_blank" onsubmit="return false;">
<button class="accordion">Section 1</button>
  <div class="panel" align="center">
    <label style="width: 90px;">Basic Arithmetic</label>
    <br/>
    <label style="width: 90px;">Value 1</label>&nbsp;
    <input type="text" name="Value1" id="Value1" value="" style="width:150px; font-size: 18px;line-height: 25px; height: 20px;"/>
    <br/>
    <label style="width: 90px;">Value 2</label>&nbsp;
    <input type="text" name="Value2" id="Value2" value="" style="width:150px; font-size: 18px;line-height: 25px; height: 20px;"/>
    <br/>
    <table>
      <tr>
	    <td>
          <input type="button" id="btnAdd" value="+" class="button" style="width:50px"/>
        </td>
	    <td>
          <input type="button" id="btnSubtract" value="-" class="button" style="width:50px"/>
        </td>
	    <td>
          <input type="button" id="btnMultiply" value="*" class="button" style="width:50px"/>
        </td>
	    <td>
          <input type="button" id="btnDivide" value="/" class="button" style="width:50px"/>
        </td>
	  </tr>
    </table>
    <label style="width: 90px;">Final Value</label>&nbsp;
    <input type="text" name="FinalValue" id="FinalValue" value="" style="width:150px; font-size: 18px;line-height: 25px; height: 20px;"/>
  </div>
<button class="accordion">Section 2</button>
<div class="panel" align='center'>
	<h1>Basic Arithmetic</h1>
    <br/>
	<label for="usr">Value 1</label>
	<input type="text" class="form-control" name="Value3" id="Value3" value="" style="width:250px"/>
	<br/>
	<label for="usr">Value 2</label>
	<input type="text" class="form-control" name="Value4" id="Value4" value="" style="width:250px"/>
	<br/>
	<table>
      <tr>
	    <td>
		  <button type="button" class="btn btn-primary" id="btnAdd2" style="width:50px">+</button>&nbsp;
        </td>
	    <td>
		  <button type="button" class="btn btn-success" id="btnSubtract2" style="width:50px">-</button>&nbsp;
        </td>
	    <td>
		  <button type="button" class="btn btn-info" id="btnMultiply2" style="width:50px">*</button>&nbsp;
        </td>
	    <td>
		  <button type="button" class="btn btn-danger" id="btnDivide2" style="width:50px">/</button>
        </td>
	  </tr>
    </table>
	<label for="usr">Final Value</label>
	<input type="text" class="form-control" name="FinalValue2" id="FinalValue2" style="width:250px">
</div>
</form>
<script src="../lib/jquery-3.2.1.min.js"></script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
<script>
var urlWorker = 'arithmeticClass.php';
$(function() {
	function ProcessRequest(data,gotoProcess,targetId,Dim){
        //var isDim = (Dim == undefined) ? true : false;
        $.ajax({
            type: 'POST',
            dataType : 'json',
            timeout : 300000, // 5 minutes
            url: urlWorker,
            data: data,
			//async: false,
            success: function(data){
				console.log('success');
                //if (typeof gotoProcess == 'function') {
                    gotoProcess(data);
                //}else{
                //    var theFunction = eval('(' + gotoProcess + ')');
                //    theFunction(data);
                //}	
            },
            //beforeSend: function(){
                //if(isDim)
                    //$.dimScreen(100, 0.5, function() {	$('#content').fadeIn();	});
            //},
            //complete: function(jqXHR){
                //if(isDim)
                    //$.dimScreenStop();
            //},
            error : function(jqXHR){
				console.log('error');
				console.log(jqXHR);
				console.log(jqXHR.statusText);
                //errorLogs('<span class="error">&mdash; '+ucwords(jqXHR.statusText)+' Request</span><br/>');
            }
        });
    }
	
	$("#btnAdd").click(function(){
		var Value1 = $.trim($('#Value1').val());
		var Value2 = $.trim($('#Value2').val());
		
		ProcessRequest('fn=add&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				//msgbox('<p class="success">'+res.message+'</p>','Alert Message');
				$('#FinalValue').val(res.message);
            }else{
				alert('error');
				//msgbox('<p class="error">'+res.message+'</p>','Alert Message');
            }
        });
    });
	
	$("#btnSubtract").click(function(){
		var Value1 = $.trim($('#Value1').val());
		var Value2 = $.trim($('#Value2').val());
		
		ProcessRequest('fn=subtract&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				$('#FinalValue').val(res.message);
            }else{
				alert('error');
            }
        });
    });
	
	$("#btnMultiply").click(function(){
		var Value1 = $.trim($('#Value1').val());
		var Value2 = $.trim($('#Value2').val());
		
		ProcessRequest('fn=multiply&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				$('#FinalValue').val(res.message);
            }else{
				alert('error');
            }
        });
    });
	
	$("#btnDivide").click(function(){
		var Value1 = $.trim($('#Value1').val());
		var Value2 = $.trim($('#Value2').val());
		
		ProcessRequest('fn=divide&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				$('#FinalValue').val(res.message);
            }else{
				alert('error');
            }
        });
    });
	
	
	$("#btnAdd2").click(function(){
		var Value1 = $.trim($('#Value3').val());
		var Value2 = $.trim($('#Value4').val());
		
		ProcessRequest('fn=add&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				//msgbox('<p class="success">'+res.message+'</p>','Alert Message');
				$('#FinalValue2').val(res.message);
            }else{
				alert('error');
				//msgbox('<p class="error">'+res.message+'</p>','Alert Message');
            }
        });
    });
	
	$("#btnSubtract2").click(function(){
		var Value1 = $.trim($('#Value3').val());
		var Value2 = $.trim($('#Value4').val());
		
		ProcessRequest('fn=subtract&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				$('#FinalValue2').val(res.message);
            }else{
				alert('error');
            }
        });
    });
	
	$("#btnMultiply2").click(function(){
		var Value1 = $.trim($('#Value3').val());
		var Value2 = $.trim($('#Value4').val());
		
		ProcessRequest('fn=multiply&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				$('#FinalValue2').val(res.message);
            }else{
				alert('error');
            }
        });
    });
	
	$("#btnDivide2").click(function(){
		var Value1 = $.trim($('#Value3').val());
		var Value2 = $.trim($('#Value4').val());
		
		ProcessRequest('fn=divide&Value1='+Value1+'&Value2='+Value2,function(res){
			if (res.success){
				$('#FinalValue2').val(res.message);
            }else{
				alert('error');
            }
        });
    });
});
</script>